const express = require('express');

const app = express();

app.use('/static', express.static(`${__dirname}/public/static`));
app.get('/', (req, res) => {
  res.sendFile(`${__dirname}/public/index.html`);
});

const port = 3000;
app.listen(port);
console.log(`server started and available at http:localhost:${port}/`);
