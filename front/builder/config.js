'use strict';

const config = {};

config.srcDir = '../src';
config.srcDirJs = `${config.srcDir}/app`;
config.staticDir = '../../public/static';

config.paths = {
  in: {
    js: [
      `${config.srcDirJs}/**/*.module.js`,
      `${config.srcDirJs}/**/*.config.js`,
      `${config.srcDirJs}/**/*.service.js`,
      `${config.srcDirJs}/**/*.controller.js`,
      `${config.srcDirJs}/**/*.directive.js`,
    ],
  },
  out: {
    js: `${config.staticDir}/app`,
  },
};

config.babel = {
  presets: ['es2015'],
  plugins: ['transform-remove-strict-mode'],
};

module.exports = config;
