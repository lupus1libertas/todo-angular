'use strict';

const gulp = require('gulp');
const plugins = require('gulp-load-plugins')();
const config = require('./config');

// tasks
gulp.task('make-app-js', makeAppJs(gulp, config, plugins));
gulp.task('make-app-js-min', makeAppJsMin(gulp, config, plugins));

// watchers
gulp.task('watch-js', () => gulp.watch(config.paths.in.js, ['make-app-js']));

// default
gulp.task('default', ['watch-js']);
// gulp.task('default', ['make-app-js']);

// function definitions
function makeAppJs(gulp, config, plugins) {
  return () => gulp.src(config.paths.in.js)
    .pipe(plugins.babel(config.babel))
    .pipe(plugins.concat('app.js'))
    .pipe(gulp.dest(config.paths.out.js));
}

function makeAppJsMin(gulp, config, plugins) {
  return () => makeAppJs(gulp, config, plugins)()
    .pipe(plugins.uglify())
    .pipe(plugins.concat('app.min.js'))
    .pipe(gulp.dest(config.paths.out.js));
}
