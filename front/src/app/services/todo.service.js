(() => {
  'use strict';

  angular.module('app').factory('todoService', todoService);
  todoService.$inject = ['$q', '$localStorage'];

  function todoService($q, $localStorage) {
    const self = this;
    self.getThings = getThings;
    self.removeThing = removeThing;
    self.addThing = addThing;
    self.updateThing = updateThing;

    initStorage();

    return self;

    //---
    function initStorage() {
      if ($localStorage.things) return;

      $localStorage.thingIdCounter = 4;
      $localStorage.things = new Things([
        { id: 1, text: 'Buy milk', status: 'active' },
        { id: 2, text: 'Conquer the World', status: 'active' },
        { id: 3, text: 'Meet grandma', status: 'active' },
      ]);
    }

    function getThings(page = 0) {
      // NOTE: we need use angular promise instead vanilla js
      // because it contains dirty check and so
      // we don't need provide $scope.apply to our then callbacks
      return $q((resolve, reject) => {
        resolve(new Things($localStorage.things));
      });
      // return new Promise((resolve, reject) => {
      //   const things = [
      //     { id: ++i, text: 'apples' },
      //     { id: ++i, text: 'Buy milk' },
      //     { id: ++i, text: 'Conquer the World' },
      //     { id: ++i, text: 'Meet grandma' },
      //     { id: ++i, text: 'test1' },
      //     { id: ++i, text: 'test2' },
      //     { id: ++i, text: 'test3' },
      //   ];
      //   resolve(things);
      // });
    }

    function updateThing(thing) {
      return $q((resolve, reject) => {
        const thingId = thing.id;

        $localStorage.things[thingId] = thing;
        resolve($localStorage.things[thingId]);
      });
    }

    function removeThing(thingId) {
      return $q((resolve, reject) => {
        if (!thingId) reject();

        delete $localStorage.things[thingId];
        resolve({ deleted: true, id: thingId });
      });
    }

    function addThing(text) {
      return $q((resolve, reject) => {
        if (!text) reject();

        const id = $localStorage.thingIdCounter++;
        const status = 'active';
        const thing = { id, text, status };
        $localStorage.things[id] = thing;
        resolve(thing);
      });
    }
  }

  class Things {
    constructor(values) {
      Object.keys(values).forEach((key) => {
        this[key] = values[key];
      });

      this.toArray = toArray;

      Object.defineProperties(this, {
        toArray: {
          enumerable: false,
        },
      });

      //---
      function toArray() {
        return Object.keys(this).map((key) => {
          return { id: key, text: this[key].text, status: this[key].status };
        });
      }
    }
  }
})();
