(() => {
  'use strict';

  angular.module('app').controller('TodoController', TodoController);
  TodoController.$inject = ['$scope', 'todoService'];

  function TodoController($scope, todoService) {
    const todo = this;
    todo.filterMode = 'active';
    todo.things = [];
    todo.removeThing = removeThing;
    todo.processThing = processThing;
    todo.clickFilter = clickFilter;
    todo.thingIsFinished = thingIsFinished;
    todo.thingIsVisible = thingIsVisible;
    todo.filterIsSelected = filterIsSelected;

    $scope.$on('TodoInputFormController:add-thing', onFormInputThingAdd);

    getThings();

    //---
    function filterIsSelected(filterName) {
      return todo.filterMode === filterName;
    }

    function onFormInputThingAdd(event, data) {
      addThing(data.thingText);
    }

    function thingIsVisible(thingStatus) {
      return todo.filterMode === thingStatus || todo.filterMode === 'all';
    }

    function thingIsFinished(thingStatus) {
      return thingStatus === 'finished';
    }

    function clickFilter(filterMode) {
      todo.filterMode = filterMode;
    }

    function addThing(thingText) {
      if (!thingText) return;

      todoService.addThing(thingText).then((thing) => {
        getThings();
      }).catch(() => {
        alert('Error when addNewThing');
      });
    }

    function getThings() {
      todoService.getThings().then((things) => {
        todo.things = things.toArray();
        // $scope.apply();
      }).catch((err) => {
        alert('Error on getting things');
      });
    }

    function removeThing(thingId) {
      todoService.removeThing(thingId).then((result) => {
        getThings();
      });
    }

    function processThing(thing) {
      thing.status = thing.status === 'active' ? 'finished' : 'active';

      todoService.updateThing(thing)
        .then((thing) => {
          getThings();
        })
        .catch(() => {
          alert('Error on finish thing');
        });
    }
  }
})();
