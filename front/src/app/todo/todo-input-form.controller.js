(() => {
  'use strict';

  angular.module('app').controller('TodoInputFormController', TodoInputFormController);
  TodoInputFormController.$inject = ['$scope'];

  function TodoInputFormController($scope) {
    const todoForm = this;
    todoForm.newThingText = '';
    todoForm.addThingFromBuffer = addThingFromBuffer;
    todoForm.onKeyDown = onKeyDown;

    //---
    function onKeyDown(event) {
      const enterKey = 13;
      if (event.keyCode === enterKey) {
        addThingFromBuffer();
      }
    }

    function addThingFromBuffer() {
      $scope.$emit('TodoInputFormController:add-thing', { thingText: todoForm.newThingText });
      todoForm.newThingText = '';
    }
  }
})();
