(function () {
  'use strict';

  angular.module('app', ['ngStorage']);
})();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(function () {
  'use strict';

  angular.module('app').factory('todoService', todoService);
  todoService.$inject = ['$q', '$localStorage'];

  function todoService($q, $localStorage) {
    var self = this;
    self.getThings = getThings;
    self.removeThing = removeThing;
    self.addThing = addThing;
    self.updateThing = updateThing;

    initStorage();

    return self;

    //---
    function initStorage() {
      if ($localStorage.things) return;

      $localStorage.thingIdCounter = 4;
      $localStorage.things = new Things([{ id: 1, text: 'Buy milk', status: 'active' }, { id: 2, text: 'Conquer the World', status: 'active' }, { id: 3, text: 'Meet grandma', status: 'active' }]);
    }

    function getThings() {
      var page = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;

      // NOTE: we need use angular promise instead vanilla js
      // because it contains dirty check and so
      // we don't need provide $scope.apply to our then callbacks
      return $q(function (resolve, reject) {
        resolve(new Things($localStorage.things));
      });
      // return new Promise((resolve, reject) => {
      //   const things = [
      //     { id: ++i, text: 'apples' },
      //     { id: ++i, text: 'Buy milk' },
      //     { id: ++i, text: 'Conquer the World' },
      //     { id: ++i, text: 'Meet grandma' },
      //     { id: ++i, text: 'test1' },
      //     { id: ++i, text: 'test2' },
      //     { id: ++i, text: 'test3' },
      //   ];
      //   resolve(things);
      // });
    }

    function updateThing(thing) {
      return $q(function (resolve, reject) {
        var thingId = thing.id;

        $localStorage.things[thingId] = thing;
        resolve($localStorage.things[thingId]);
      });
    }

    function removeThing(thingId) {
      return $q(function (resolve, reject) {
        if (!thingId) reject();

        delete $localStorage.things[thingId];
        resolve({ deleted: true, id: thingId });
      });
    }

    function addThing(text) {
      return $q(function (resolve, reject) {
        if (!text) reject();

        var id = $localStorage.thingIdCounter++;
        var status = 'active';
        var thing = { id: id, text: text, status: status };
        $localStorage.things[id] = thing;
        resolve(thing);
      });
    }
  }

  var Things = function Things(values) {
    var _this = this;

    _classCallCheck(this, Things);

    Object.keys(values).forEach(function (key) {
      _this[key] = values[key];
    });

    this.toArray = toArray;

    Object.defineProperties(this, {
      toArray: {
        enumerable: false
      }
    });

    //---
    function toArray() {
      var _this2 = this;

      return Object.keys(this).map(function (key) {
        return { id: key, text: _this2[key].text, status: _this2[key].status };
      });
    }
  };
})();
(function () {
  'use strict';

  angular.module('app').controller('TodoInputFormController', TodoInputFormController);
  TodoInputFormController.$inject = ['$scope'];

  function TodoInputFormController($scope) {
    var todoForm = this;
    todoForm.newThingText = '';
    todoForm.addThingFromBuffer = addThingFromBuffer;
    todoForm.onKeyDown = onKeyDown;

    //---
    function onKeyDown(event) {
      var enterKey = 13;
      if (event.keyCode === enterKey) {
        addThingFromBuffer();
      }
    }

    function addThingFromBuffer() {
      $scope.$emit('TodoInputFormController:add-thing', { thingText: todoForm.newThingText });
      todoForm.newThingText = '';
    }
  }
})();
(function () {
  'use strict';

  angular.module('app').controller('TodoController', TodoController);
  TodoController.$inject = ['$scope', 'todoService'];

  function TodoController($scope, todoService) {
    var todo = this;
    todo.filterMode = 'active';
    todo.things = [];
    todo.removeThing = removeThing;
    todo.processThing = processThing;
    todo.clickFilter = clickFilter;
    todo.thingIsFinished = thingIsFinished;
    todo.thingIsVisible = thingIsVisible;
    todo.filterIsSelected = filterIsSelected;

    $scope.$on('TodoInputFormController:add-thing', onFormInputThingAdd);

    getThings();

    //---
    function filterIsSelected(filterName) {
      return todo.filterMode === filterName;
    }

    function onFormInputThingAdd(event, data) {
      addThing(data.thingText);
    }

    function thingIsVisible(thingStatus) {
      return todo.filterMode === thingStatus || todo.filterMode === 'all';
    }

    function thingIsFinished(thingStatus) {
      return thingStatus === 'finished';
    }

    function clickFilter(filterMode) {
      todo.filterMode = filterMode;
    }

    function addThing(thingText) {
      if (!thingText) return;

      todoService.addThing(thingText).then(function (thing) {
        getThings();
      }).catch(function () {
        alert('Error when addNewThing');
      });
    }

    function getThings() {
      todoService.getThings().then(function (things) {
        todo.things = things.toArray();
        // $scope.apply();
      }).catch(function (err) {
        alert('Error on getting things');
      });
    }

    function removeThing(thingId) {
      todoService.removeThing(thingId).then(function (result) {
        getThings();
      });
    }

    function processThing(thing) {
      thing.status = thing.status === 'active' ? 'finished' : 'active';

      todoService.updateThing(thing).then(function (thing) {
        getThings();
      }).catch(function () {
        alert('Error on finish thing');
      });
    }
  }
})();